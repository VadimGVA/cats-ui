import { error } from '../../../pages/rating-names/__mocks__/error';
import { RatingNamePage, ratingNamePageFixture } from '../__page-object__';
import { test as base } from 'playwright/test';
import { expect } from '@playwright/test';

const test = base.extend<{ ratingNamePage: RatingNamePage }>({
  ratingNamePage: ratingNamePageFixture,
});

test('При ошибке сервера в методе rating - отображается попап ошибки', async ({
  page,
}) => {
  await page.route(
    request => request.href.includes('/rating'),
    async route => {
      await route.fulfill({
        json: error,
      });
    }
  );
  await page.goto('/rating');
  await expect(page.getByText('Internal Server Error')).toBeVisible();
});

test('Проверить отображение рейтинга котиков', async ({ ratingNamePage }) => {
  await ratingNamePage.openRatingPage();
  await ratingNamePage.checkRating();
});

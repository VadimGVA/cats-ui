export const error = {
  data: 'Error: Bad param order',
  isServer: 'true',
  output: {
    statusCode: 500,
    payload: {
      statusCode: 500,
      error: 'Internal Server Error',
      message: 'An internal server error occurred',
    },
    headers: {},
  },
};

import {expect, Page, TestFixture} from '@playwright/test';
import {test} from '@playwright/test'


export class RatingNamePage {
    private page: Page;

    constructor({
                    page,
                }: {
        page: Page;
    }) {
        this.page = page;
    }


    async openRatingPage() {
        return await test.step('Открыть страницу рейтинга', async () => {
            await this.page.goto('/rating')
        })
    }

    async checkRating() {
        return await test.step(`Проверить корректность отображения рейтинга`, async () => {

            const ratingRowsLength = await this.page.$$eval('//*[@class="rating-names_table__Jr5Mf"][1]/*', tbody => {
                tbody.forEach((c, i) => console.log(i, c.className, c.attributes, Object.keys(c)))
                return Promise.resolve(tbody[0].childNodes.length)
            })

            for (let i = 1; i <= ratingRowsLength -1; i++) {
                const firstNumber = await this.page.$eval(`//table[@class="rating-names_table__Jr5Mf"][1]//tr[${i}]/td[3]`, element => parseInt(element.textContent));
                const secondNumber = await this.page.$eval(`//table[@class="rating-names_table__Jr5Mf"][1]//tr[${i + 1}]/td[3]`, element => parseInt(element.textContent));
                expect(firstNumber).toBeGreaterThanOrEqual(secondNumber);
            }
        })
    }
}

export type RatingNamesPageFixture = TestFixture<
    RatingNamePage,
    {
        page: Page;
    }
>;

export const ratingNamePageFixture: RatingNamesPageFixture = async (
    {page},
    use
) => {
    const ratingNamePage = new RatingNamePage({page});

    await use(ratingNamePage);
};

